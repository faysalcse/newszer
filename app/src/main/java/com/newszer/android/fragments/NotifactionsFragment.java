package com.newszer.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aapbd.appbajarlib.network.NetInfo;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.newszer.android.NotificationAdapter;
import com.newszer.R;
import com.newszer.android.model.NetModel.NewsModel;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotifactionsFragment extends Fragment {

    View view;
    ShimmerFrameLayout shimmer_view_container;
    LinearLayout mainlayoutView;
    DatabaseReference reference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notifications, container, false);
        shimmer_view_container = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);
        mainlayoutView = view.findViewById(R.id.mainlayoutView);




        if (!NetInfo.isOnline(getActivity())) {
            shimmer_view_container.setVisibility(View.VISIBLE);
            shimmer_view_container.startShimmer();
        }else {
            firebaseOperations();
        }


       // ((DrawerLocker)getActivity()).setDrawerEnabled(false);

        return view;
    }

    private void initNotificationslIst(List<NewsModel> news) {
        Collections.reverse(news);
        RecyclerView list = view.findViewById(R.id.notificationList);
        NotificationAdapter adapter = new NotificationAdapter(getActivity(), news);
        list.setAdapter(adapter);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public void firebaseOperations() {
        shimmer_view_container.setVisibility(View.VISIBLE);
        mainlayoutView.setVisibility(View.GONE);
        shimmer_view_container.stopShimmer();

        List<NewsModel> newsList = new ArrayList<>();
        newsList.clear();
        reference = FirebaseDatabase.getInstance().getReference().child("tbl_notification");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> storiesary = new ArrayList<String>();
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    //  Log.d(TAG, "onDataChange: "+childSnapshot.child("category_name").getValue());
                    NewsModel model = new NewsModel();
                    model.setCatId(childSnapshot.child("cat_id").getValue().toString());
                    model.setCategoryName(childSnapshot.child("category_name").getValue().toString());
                    model.setContentType(childSnapshot.child("content_type").getValue().toString());
                    model.setNewsDate(childSnapshot.child("news_date").getValue().toString());
                    model.setNewsDescription(childSnapshot.child("news_description").getValue().toString());
                    model.setNewsImage(childSnapshot.child("news_image").getValue().toString());
                    model.setNewsTitle(childSnapshot.child("news_title").getValue().toString());
                    model.setNewsUrl(childSnapshot.child("news_url").getValue().toString());
                    model.setYoutubeUrl(childSnapshot.child("youtube_url").getValue().toString());
                    newsList.add(model);
                }

                initNotificationslIst(newsList);
                shimmer_view_container.stopShimmer();
                shimmer_view_container.setVisibility(View.GONE);
                mainlayoutView.setVisibility(View.VISIBLE);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Snackbar snackbar = Snackbar
                        .make(mainlayoutView, "Error ! something wrong", Snackbar.LENGTH_LONG)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                firebaseOperations();
                            }
                        });

                snackbar.show();
            }
        });
    }
}
