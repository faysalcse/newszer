package com.newszer.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.adapter.NewsImageAdapter;
import com.newszer.android.adapter.SkillsAdapter;
import com.newszer.android.adapter.SubCateAdpters;
import com.newszer.android.adapter.TrendingAdpers;
import com.newszer.android.model.Places;
import com.newszer.android.model.SubCate;

import java.util.ArrayList;
import java.util.List;

public class SomeThingFragment extends Fragment {

    View view;
    List<Places> userData;
    RecyclerView userListView;
    RecyclerView placesListView;
    EditText EtSearch;
    NewsImageAdapter adapter;
    TrendingAdpers trendingAdapter;

    RecyclerView subCategoryList;
    SubCateAdpters subCateAdpter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
         view=inflater.inflate(R.layout.fragment_somethings,container,false);

         userData=new ArrayList<>();
         userData.add(new Places("","Thanos is a fictional supervillain appearing in American comic books published by Marvel Comics. The character, created by writer/artist Jim Starlin, first ...","https://images.pexels.com/photos/572861/pexels-photo-572861.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500","Dhaka","Bangladesh"));
         userData.add(new Places("","","https://images.pexels.com/photos/934964/pexels-photo-934964.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500","Gazipur","Bangladesh"));
         userData.add(new Places("","","https://images.pexels.com/photos/1666021/pexels-photo-1666021.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500","Noyakhali","Bangladesh"));
         userData.add(new Places("","","https://images.pexels.com/photos/70365/forest-sunbeams-trees-sunlight-70365.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500","Rongpur","Bangladesh"));
         userData.add(new Places("","","https://images.pexels.com/photos/799443/pexels-photo-799443.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500","Rajsahi","Bangladesh"));
         userData.add(new Places("","","https://images.pexels.com/photos/561463/pexels-photo-561463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260","Pabna","Bangladesh"));
         userData.add(new Places("","","https://images.pexels.com/photos/1173777/pexels-photo-1173777.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500","Cox'z Bazar","Bangladesh"));


        userListView=(RecyclerView)view.findViewById(R.id.citiesListView);
        adapter=new NewsImageAdapter(getActivity(),userData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        userListView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        userListView.setItemAnimator(new DefaultItemAnimator());
        userListView.setAdapter(adapter);
        SubCateAdapterView(view);
        trandingAdapterView(view);
        skillViews(view);
        return view;
    }

    private void trandingAdapterView(View view){

        List<String> trandingList=new ArrayList<>();
        trandingList.add("https://images.pexels.com/photos/934964/pexels-photo-934964.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");
        trandingList.add("https://images.pexels.com/photos/1666021/pexels-photo-1666021.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");
        trandingList.add("https://images.pexels.com/photos/70365/forest-sunbeams-trees-sunlight-70365.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");
        trandingList.add("https://images.pexels.com/photos/799443/pexels-photo-799443.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");
        trandingList.add("https://images.pexels.com/photos/561463/pexels-photo-561463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
        trandingList.add("https://images.pexels.com/photos/1173777/pexels-photo-1173777.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");


        placesListView=(RecyclerView)view.findViewById(R.id.placesListView);
        trendingAdapter=new TrendingAdpers(getActivity(),trandingList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        placesListView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        placesListView.setItemAnimator(new DefaultItemAnimator());
        placesListView.setAdapter(trendingAdapter);
    }

    private void skillViews(View view){
        // Choose your own preferred column width
        final List<String> items = new ArrayList<>();
        items.add("Photoshop");
        items.add("java");
        items.add("Android");
        items.add("Windows");
        items.add("add");


        // initialize your items array
        SkillsAdapter adapter = new SkillsAdapter(getActivity(), items);

        GridView gridView=(GridView)view.findViewById(R.id.skillsGrid);
        gridView.setAdapter(adapter);
    }

    private void SubCateAdapterView(View view){

        List<SubCate> subCateListData=new ArrayList<>();
        subCateListData.add(new SubCate("Apsaaraap","https://st.depositphotos.com/1010146/3953/i/950/depositphotos_39531879-stock-photo-loudly-laughing-guy-over-white.jpg"));
        subCateListData.add(new SubCate("Yahoo India","https://yt3.ggpht.com/a-/AAuE7mBm_bo-8RkcuNhAwIi2E9nPDqS4pZeGPBOoMA=s900-mo-c-c0xffffffff-rj-k-no"));
        subCateListData.add(new SubCate("I am Happy","https://images.pexels.com/photos/70365/forest-sunbeams-trees-sunlight-70365.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"));
        subCateListData.add(new SubCate("FilterCopy","https://images.pexels.com/photos/799443/pexels-photo-799443.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"));
        subCateListData.add(new SubCate("Radio2fun","https://images.pexels.com/photos/561463/pexels-photo-561463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"));
        subCateListData.add(new SubCate("Blaa Blaa","https://images.pexels.com/photos/1173777/pexels-photo-1173777.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"));

        subCategoryList=(RecyclerView)view.findViewById(R.id.subCategoryList);
        subCateAdpter=new SubCateAdpters(getActivity(),subCateListData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        subCategoryList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        subCategoryList.setItemAnimator(new DefaultItemAnimator());
        subCategoryList.setAdapter(subCateAdpter);
    }

}
