package com.newszer.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aapbd.appbajarlib.network.NetInfo;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.newszer.R;
import com.newszer.android.adapter.CategoryViewAdapter;
import com.newszer.android.adapter.SubCateAdpters;
import com.newszer.android.model.NetModel.Category;
import com.newszer.android.model.SubCate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anupamchugh on 10/12/15.
 */
public class ListFragment extends Fragment {

    public static final String TAG="CategoryFragmentOPS";

    RecyclerView categoryList;
    Context context;
    RecyclerView subCategoryList;
    SubCateAdpters subCateAdpter;
    LinearLayout viewLayout;
    LinearLayout loadingLayout;
    LinearLayout errorLayoput;
    ShimmerFrameLayout shimmer;
    LinearLayout listLayout;

    public ListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        rootView.setClickable(true);
        context=getActivity();




        categoryList=rootView.findViewById(R.id.categoryList);
        viewLayout=rootView.findViewById(R.id.viewLayout);
        loadingLayout=rootView.findViewById(R.id.loadingLayout);
        errorLayoput=rootView.findViewById(R.id.errorLayoput);
        listLayout=rootView.findViewById(R.id.listLayout);

        shimmer = (ShimmerFrameLayout)rootView.findViewById(R.id.shimmer_view_container);

        SubCateAdapterView(rootView);

        if (!NetInfo.isOnline(context)) {
           loadingLayout.setVisibility(View.VISIBLE);
           shimmer.startShimmer();
        }else {
            firebaseOperations();
        }


        return rootView;
    }

    private void firebaseOperations(){
        loadingLayout.setVisibility(View.VISIBLE);
        shimmer.startShimmer();
        List<Category> datas=new ArrayList<>();
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference().child("tbl_category");

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()){

                    Category categories=new Category();
                    categories.setCid(childSnapshot.getKey());
                    categories.setCategoryName(childSnapshot.child("category_name").getValue().toString());
                    categories.setCategoryImage(childSnapshot.child("category_image").getValue().toString());

                    Log.d(TAG, "Category Name"+childSnapshot.child("category_name").getValue().toString()+"Category Image : "+childSnapshot.child("category_image").getValue().toString());

                    if (!categories.getCid().equals("-LhdZM4G9WiNp6h8s2m2")){
                        datas.add(categories);
                    }

                }

                loadingLayout.setVisibility(View.GONE);
                showList(datas);



            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                loadingLayout.setVisibility(View.VISIBLE);
                shimmer.stopShimmer();
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showList(List<Category> categories){
        viewLayout.setVisibility(View.VISIBLE);
        CategoryViewAdapter adapter=new CategoryViewAdapter(getActivity(),categories);
        categoryList.setLayoutManager(new LinearLayoutManager(getActivity()));
        categoryList.setAdapter(adapter);


    }

    private void SubCateAdapterView(View view){

        List<SubCate> subCateListData=new ArrayList<>();
        subCateListData.add(new SubCate("Apsaaraap","https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/8b/3d/73/8b3d7300-6a18-7c63-67f1-12d81aba4675/AppIcon-0-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"));
        subCateListData.add(new SubCate("Yahoo India","https://yt3.ggpht.com/a-/AAuE7mBm_bo-8RkcuNhAwIi2E9nPDqS4pZeGPBOoMA=s900-mo-c-c0xffffffff-rj-k-no"));
        subCateListData.add(new SubCate("I am Happy","https://www.thewrap.com/wp-content/uploads/2014/07/bbc-news-logo.jpg"));
        subCateListData.add(new SubCate("FilterCopy","https://www.soomfoods.com/wp-content/uploads/2013/06/WSJ-logo-copy.png"));
        subCateListData.add(new SubCate("Radio2fun","https://www.afsc.org/sites/default/files/styles/maxsize/public/images/NBC%20News.png?itok=QyB1uaGb"));
        subCateListData.add(new SubCate("Blaa Blaa","https://d12jofbmgge65s.cloudfront.net/blog/wp-content/uploads/2019/04/Fox-news-logo.png"));

        subCategoryList=(RecyclerView)view.findViewById(R.id.subCategoryList);
        subCateAdpter=new SubCateAdpters(getActivity(),subCateListData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        subCategoryList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        subCategoryList.setItemAnimator(new DefaultItemAnimator());
        subCategoryList.setAdapter(subCateAdpter);

    }




}
