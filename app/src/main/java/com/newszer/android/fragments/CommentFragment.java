package com.newszer.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.adapter.CommentListAdapter;
import com.newszer.android.model.Comments;

import java.util.ArrayList;
import java.util.List;

public class CommentFragment extends Fragment {
    View view;
    RecyclerView commentList;
    Context context;
    EditText getComment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_comments,container,false);
        context=getActivity();
        commentList=view.findViewById(R.id.commentList);

        List<Comments> comments=new ArrayList<>();
        comments.add(new Comments("Eric Edquist","https://scontent.fdac16-1.fna.fbcdn.net/v/t1.0-9/12540522_10207501481671881_1043369682091858694_n.jpg?_nc_cat=107&_nc_oc=AQl_ITT5IM7vsmg8TRRYh8nle1FEEcy7C7ZtjmvLq4ErRFaURqphXzJ63UbWzTJrj6M&_nc_ht=scontent.fdac16-1.fna&oh=66675885fe507e5a1aee725baf1123ab&oe=5DBBBED7","Fascinating. Thank you, NewsZer for posting this","1","50","7"));
        comments.add(new Comments("Lana Borges","https://scontent.fdac16-1.fna.fbcdn.net/v/t1.0-9/48419428_10158024110212802_2945945401498271744_n.jpg?_nc_cat=105&_nc_oc=AQl6kqtOEdZcUC0WCzvHTzlWof_4rRISS93wwJvHcsPSwF6RWw2N2eBQIVU1v10q8BA&_nc_ht=scontent.fdac16-1.fna&oh=a913860465f71e213df7b702ce27e28d&oe=5DBA3BF2","There is a say, u can't run a race with an ass instead of horse..... it's suitable for this app.","5","17","0"));
        comments.add(new Comments("Darren Ramshaw","https://scontent.fdac16-1.fna.fbcdn.net/v/t1.0-9/62476180_2254734194841817_3502670383939584_n.jpg?_nc_cat=103&_nc_oc=AQnAEYz2UGW1fEbV2wsYZbv9hP9X0PhhN-XY4gFKOKOleLpdj2KA7LvNja-I88sEza4&_nc_ht=scontent.fdac16-1.fna&oh=699e3b5116b39adb1d01855e760bd74f&oe=5DABA210","My word can’t describe it","2","22","7"));
        comments.add(new Comments("Pat Woolfenden","https://scontent.fdac16-1.fna.fbcdn.net/v/t1.0-9/13912766_894130960730544_5693121072107707869_n.jpg?_nc_cat=111&_nc_oc=AQk21cxq7WdOWEoe_vZvlPSDRloKh4c9PpAQxQIdXrF_nLMVYb79hkCobizn5zAynws&_nc_ht=scontent.fdac16-1.fna&oh=862f1c76e31898c3eaf0992b25ebe4f6&oe=5DB1CB76","I didn’t know about this.","5","38","2"));
        comments.add(new Comments("Mina Frank","https://scontent.fdac16-1.fna.fbcdn.net/v/t1.0-9/66463716_2229309557106449_140343639854284800_n.jpg?_nc_cat=111&_nc_oc=AQncLC9zcoqp3zIjl-ACgGc883h1BTg373BTqzu6BOVadxsyKqR4dUkTp7tKX00SpzI&_nc_ht=scontent.fdac16-1.fna&oh=ff8a8b7676787e85979649a839e07f03&oe=5DB2F25D","What a brave coast guard","2","74","6"));


        showList(comments);

        getComment=view.findViewById(R.id.getComment);
        getComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // ((MainActivity)getActivity()).setKeyTypeMode();
            }
        });


        return view;
    }

    private void showList(List<Comments> categories){
        CommentListAdapter adapter=new CommentListAdapter(getActivity(),categories);
        commentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        commentList.setAdapter(adapter);
    }
}
