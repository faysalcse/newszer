package com.newszer.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.newszer.R;
import com.newszer.android.adapter.TrandingNewsAdapter;
import com.newszer.android.model.NetModel.NewsModel;

import java.util.List;

public class TrendingFragment extends Fragment {

    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_tranding,container,false);
        return view;
    }

    public void trendingAdapterViews(List<NewsModel> news){

        RecyclerView userListView=(RecyclerView)view.findViewById(R.id.trendingList);
        TrandingNewsAdapter adapter=new TrandingNewsAdapter(getActivity(),news);
        userListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userListView.setAdapter(adapter);
    }
}
