package com.newszer.android.fragments;


import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.newszer.R;
import com.newszer.android.model.News;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.List;

public class MainFragments extends Fragment {

    private static final float DEFAULT_CURRENT_PAGE_SCALE = 0.8f;
    private int dimen;

    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.main_frame_layout,container,false);
        initui(view);
        intVP();
        return view;
    }

    private void intVP(){

        final List<News> newsData=new ArrayList<>();
        newsData.add(new News("Ronaldo 'annoyed' by constant criticism"
                ,"Cristiano Ronaldo says he is \"annoyed\" that he is forced to prove that he is a good player every year and wants critics to leave him alone.\n" +
                "\n" +
                "The Juventus and Portugal star has long been regarded as one of the best players the game has ever seen, having won five Ballons d'Or and as many Champions League titles.\n" +
                "\n" +
                "After winning the Serie A title with Juve this season, the 34-year-old has now won league crowns in three different countries and is challenging for the Italian division's top scorer award having scored 21 times in 29 appearances."
                ,"Sports,Entertainment","https://wallpapercave.com/wp/wp2336339.jpg"));
          newsData.add(new News("WATCH EXCLUSIVE VIDEOS OF MESSI"
                ,"Facebook, Inc. is an American online social media and social networking service company based in Menlo Park, California. It was founded by Mark Zuckerberg, along with fellow Harvard College students and roommates Eduardo Saverin, Andrew McCollum, Dustin Moskovitz and Chris"
                ,"Sports,Entertainment","https://fsa.zobj.net/crop.php?r=fnB3INcMHysfkTKHltOh4oBuTW_eto2LsMHUFERVcwpHe7bjbNnA1xmHdF2Gwh0RTAIxmJvKn8h7kkjGwQTE0LUJCKNvkqTSZKohw9a4WuA699m3PUSz1CqiiD7rC8ZKfWzGsnSGlI-eQaoEt2ibAJf77oZkk_dfhmUdd-zsf5UO92iMHI0sJEAwwtc"));
        newsData.add(new News("WATCH EXCLUSIVE VIDEOS OF MESSI"
                ,"Facebook, Inc. is an American online social media and social networking service company based in Menlo Park, California. It was founded by Mark Zuckerberg, along with fellow Harvard College students and roommates Eduardo Saverin, Andrew McCollum, Dustin Moskovitz and Chris"
                ,"Sports,Entertainment","https://fsa.zobj.net/crop.php?r=PoI9tdgkD4aF09R_LAVlVssXSFLDlrU3pjqKLBpai7iwVvFwn1dN2MIuEnriMuSjnSZ62QG8fhv3u43nsNonmEdx81EcxGPOgbjELRBlOcb9UJ3ziO-RkpsM537m3ArSMfGpHA6xY4zOk3xHtdQjZfieRhtC0eTVQ1ivdecrovqQ5roHopEHQ0JbLdg"));
        newsData.add(new News("WATCH EXCLUSIVE VIDEOS OF MESSI"
                ,"Facebook, Inc. is an American online social media and social networking service company based in Menlo Park, California. It was founded by Mark Zuckerberg, along with fellow Harvard College students and roommates Eduardo Saverin, Andrew McCollum, Dustin Moskovitz and Chris"
                ,"Sports,Entertainment","https://fsa.zobj.net/crop.php?r=1Ryz_oDy26Qh62KLRfgo48JW34cPtRUeTm_gxtoY-2fLSWs-GIQDRvA8AUsy5VkRbOewr6D4CPRydwBSOLoVCCt7v2pqiH-38er5X-EZobmkWtO5vkquPRgKmTfU6uHS4av7bx9AvixmyLGLoEGek1idiSg9N_cA3z5gd7-Xw8Os3J17eZ9Vfq0JdZQ"));
        newsData.add(new News("WATCH EXCLUSIVE VIDEOS OF MESSI"
                ,"Facebook, Inc. is an American online social media and social networking service company based in Menlo Park, California. It was founded by Mark Zuckerberg, along with fellow Harvard College students and roommates Eduardo Saverin, Andrew McCollum, Dustin Moskovitz and Chris"
                ,"Sports,Entertainment","https://fsa.zobj.net/crop.php?r=fnB3INcMHysfkTKHltOh4oBuTW_eto2LsMHUFERVcwpHe7bjbNnA1xmHdF2Gwh0RTAIxmJvKn8h7kkjGwQTE0LUJCKNvkqTSZKohw9a4WuA699m3PUSz1CqiiD7rC8ZKfWzGsnSGlI-eQaoEt2ibAJf77oZkk_dfhmUdd-zsf5UO92iMHI0sJEAwwtc"));
        newsData.add(new News("WATCH EXCLUSIVE VIDEOS OF MESSI"
                ,"Facebook, Inc. is an American online social media and social networking service company based in Menlo Park, California. It was founded by Mark Zuckerberg, along with fellow Harvard College students and roommates Eduardo Saverin, Andrew McCollum, Dustin Moskovitz and Chris"
                ,"Sports,Entertainment","https://fsb.zobj.net/crop.php?r=ssZxjHr4wzQeDUHFLSgNp1BGWSds8CFXpGI-tdZmNAY-0hXhi-ogMXPZlXufTbCFALOHsrh7pAO-sCcp_qGhJwp2qN6OUeBWiQPOF25hSIUy6voAQFX2s8BMjZy1WO2lPo1D1zRgWSsOXG13VLfneuR6ZqNWSixGgv5ZzI8GJx383bAKUlZsxECepwc"));
        newsData.add(new News("WATCH EXCLUSIVE VIDEOS OF MESSI"
                ,"Facebook, Inc. is an American online social media and social networking service company based in Menlo Park, California. It was founded by Mark Zuckerberg, along with fellow Harvard College students and roommates Eduardo Saverin, Andrew McCollum, Dustin Moskovitz and Chris"
                ,"Sports,Entertainment","https://fsb.zobj.net/crop.php?r=7zVbT7x78m4LFameFUowMpL6ugr-dlF7hzR8xIilSZVHuB7FgyOeQHIDdib_HhCuaVoYxxf_tZ2RFjWAQKGUNx8e8ydL6tUc-x8ldUo5LnZUdq8CB1JeRq4v48yAkT61BgVIb6XEDHHS_vOUhY2x3g60Ko3vH9tFhYFzuiOZNwKnZz7bazpDi_1Qq2E"));

          /* final NewsPagerAdapter adapter=new NewsPagerAdapter(getActivity(),newsData);
            FlippableStackView stack = (FlippableStackView)view.findViewById(R.id.stack);
            stack.initStack(3, StackPageTransformer.Orientation.VERTICAL,0.9f,0.8f,1,StackPageTransformer.Gravity.CENTER);
            stack.setAdapter(adapter);
            final ImageView blurImageView=view.findViewById(R.id.exBlurImageView);*/

         /*
        ViewPager pager=view.findViewById(R.id.myVp);
      //  pager.setPageTransformer(true, new StackTransformer());
        pager.setPageTransformer(true, new StackTransformer());
       // pager.setPageTransformer(true,new ViewPagerStack());
       // pager.setPageTransformer(true, new AccordionTransformer());
        pager.setOffscreenPageLimit(2);
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                //startActivity(new Intent(getActivity(), NewsDetailsViewActivity.class).putExtra("url",adapter.returnThmbimage(position)));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/


       /* SwipeStack swipeStack = (SwipeStack)view.findViewById(R.id.swipeStack);
        swipeStack.setAdapter(new SwipeStackAdapter(newsData,getActivity()));
        swipeStack.setSwipeProgressListener(new SwipeStack.SwipeProgressListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeProgress(int position, float progress) {
                Toast.makeText(getActivity(), ""+position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });*/




    }

    private class ViewPagerStack implements ViewPager.PageTransformer {
        @Override
        public void transformPage(View page, float position) {
            if (position >= 0) {
                page.setScaleX(0.9f - 0.09f * position);
                page.setScaleY(0.9f);
                page.setTranslationX(-page.getWidth() * position);
                page.setTranslationY(30 * position);

                float rotation = -180.0F * position;
                page.setAlpha(rotation <= 90.0F && rotation >= -90.0F ? 1.0F : 0.0F);
                page.setPivotX((float)page.getWidth() * 0.5F);
                page.setPivotY((float)page.getHeight() * 0.5F);
                page.setRotationX(rotation);
            }
        }
    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;
        @Override
        public void transformPage(View page, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(view.getWidth() * -position);

                //set Y position to swipe in from top
                float yPosition = position * view.getHeight();
                view.setTranslationY(yPosition);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

    public static class BlurBuilder {
        private static final float BITMAP_SCALE = 0.4f;
        private static final float BLUR_RADIUS = 7.5f;

        public static Bitmap blur(Context context, Bitmap image) {
            int width = Math.round(image.getWidth() * BITMAP_SCALE);
            int height = Math.round(image.getHeight() * BITMAP_SCALE);

            Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
            Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

            RenderScript rs = RenderScript.create(context);
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
            Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);

            return outputBitmap;
        }
    }



    private void initui(View view){

        final View bottomSheet =view.findViewById(R.id.bottom_sheet);

        LinearLayout llBottomSheet = (LinearLayout)view.findViewById(R.id.bottom_sheet);
        final BottomSheetBehavior mBottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setHideable(false);
        setFragmentBotom(new TrendingFragment());

        // set callback for changes
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    bottomSheet.setTranslationY(slideOffset * bottomSheet.getWidth());
                }*/
            }
        });

        final TextView BtmActionTranding=bottomSheet.findViewById(R.id.BtmActionTranding);
        final TextView BtmActionComment=bottomSheet.findViewById(R.id.BtmActionComment);


        LinearLayout botomBarLayout = view.findViewById(R.id.botomBarLayout);
        botomBarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });

        BtmActionTranding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtmActionTranding.setTextColor(ContextCompat.getColor(getActivity(),R.color.black));
                BtmActionComment.setTextColor(ContextCompat.getColor(getActivity(),R.color.gray_dark));
                setFragmentBotom(new TrendingFragment());
            }
        });

        BtmActionComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtmActionComment.setTextColor(ContextCompat.getColor(getActivity(),R.color.black));
                BtmActionTranding.setTextColor(ContextCompat.getColor(getActivity(),R.color.gray_dark));
                setFragmentBotom(new CommentFragment());
            }
        });

    }

    private void setFragmentBotom(Fragment fragment){
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.replace(R.id.botomFrame, fragment);
        fragmentTransaction.commit(); // save the changes
    }


}
