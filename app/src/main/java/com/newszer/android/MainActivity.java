package com.newszer.android;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.bartoszlipinski.flippablestackview.FlippableStackView;
import com.bartoszlipinski.flippablestackview.StackPageTransformer;
import com.google.android.gms.ads.MobileAds;
import com.newszer.R;
import com.newszer.android.adapter.viewpager.NewsPagerAdapter;
import com.newszer.android.fragments.CommentFragment;
import com.newszer.android.fragments.ListFragment;
import com.newszer.android.fragments.NotifactionsFragment;
import com.newszer.android.fragments.SettingsFragment;
import java.lang.String;

import com.newszer.android.model.NetModel.Categories;
import com.newszer.android.model.NetModel.Category;
import com.newszer.android.model.NewsModes;
import com.newszer.android.rests.APIObserver;
import com.newszer.android.utils.CustomViewPagerEndSwipe;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;

import static com.newszer.android.utils.ServerOperations.callAPI;


public class MainActivity extends AppCompatActivity implements CustomViewPagerEndSwipe.OnSwipeOutListener {


    TextView todayBtn;
    DrawerLayout drawerLayout;
    LinearLayout contentLayout;
    SmartTabLayout viewPagerTab;
    ViewPager viewPagerNav;
    int viewPagePosition;

    int savePosition;

    float tempPositionOffset = 0;

    /*Action Variables*/



     BottomSheetBehavior mBottomSheetBehavior;

    float mStartDragX;

    List<NewsModes> allNewsList;
    public static final String TAG="MainActionD";
    static Context context;
    DatabaseReference reference;
    private static final float DEFAULT_CURRENT_PAGE_SCALE = 0.8f;
    private int dimen;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this);


        ButterKnife.bind(this);
        contentLayout=(LinearLayout)findViewById(R.id.contentLayout);
        context=this;
        allNewsList=new ArrayList<>();

        initUiMain();
        firebaseOperations();
        initBotomUi();
        intiTempView();



      todayBtn=findViewById(R.id.todayBtn);
      todayBtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              firebaseOperations();
          }
      });

/*

        Handler h = new Handler();

        h.postDelayed(new Runnable() {

            @Override
            public void run() {
               hideNavigation();
            }
        }, 3000);
*/



    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    private void getCategoriesData(String url){
        List<Category> datas=new ArrayList<>();
        callAPI(context,url, new APIObserver() {
            @Override
            public void completed(Object data) {
                Categories categories= new Gson().fromJson(data.toString(),Categories.class);

                for (int i=1; i<categories.getCategories().size(); i++){
                    datas.add(categories.getCategories().get(i));
                }
                //showList(datas);


            }

            @Override
            public void error(Object error) {
                contentLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void hideProgressBar() {
                contentLayout.setVisibility(View.VISIBLE);
            }
        });

    }


    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }



    private void hideNavigationBar(){
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }



    private void initDateView(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String monthname=(String) DateFormat.format("MMM", new Date());
        String dateOfMonth=(String) DateFormat.format("dd", new Date());
        String dayName=null;



        switch (day) {
            case Calendar.SATURDAY:
                dayName="SATURDAY";
                break;
            case Calendar.SUNDAY:
                dayName="SUNDAY";
                break;
            case Calendar.MONDAY:
               dayName="MONDAY";
                break;
            case Calendar.TUESDAY:
               dayName="TUESDAY";
                break;
            case Calendar.WEDNESDAY:
               dayName="WEDNESDAY";
                break;
            case Calendar.THURSDAY:
               dayName="THURSDAY";
                break;
            case Calendar.FRIDAY:
               dayName="FRIDAY";
                break;
        }

    }

    private void intiTempView(){
        int temp[]=new int[5];
        temp[0]=10;
        temp[1]=15;
        temp[2]=20;
        temp[3]=25;
        temp[4]=30;

        Random Dice = new Random();
        int n = Dice.nextInt(5);

/*

        if (n==0){
            temperatureView.setText("Temperature, "+temp[n]+"°C");
        }else if (n==1){
            temperatureView.setText("Temperature, "+temp[n]+"°C");
        }else if (n==2){
            temperatureView.setText("Temperature, "+temp[n]+"°C");
        }else if (n==3){
            temperatureView.setText("Temperature, "+temp[n]+"°C");
        }else if (n==4){
            temperatureView.setText("Temperature, "+temp[n]+"°C");
        }else {
            temperatureView.setText("Temperature, 0°C");
        }
*/

       /* if (temp[n]>= 10 && 14<=temp[n]){
            temperatureView.setText("Cold, "+temp[n]+"°C");
        }else if (temp[n]>= 15 && 20<=temp[n]){
            temperatureView.setText("Cloudy, "+temp[n]+"°C");
        }else if (temp[n]>= 21 && 25<=temp[n]){
            temperatureView.setText("Warm, "+temp[n]+"°C");
        }else {
            temperatureView.setText("Hot, "+temp[n]+"°C");
        }
*/

        System.out.println(temp[n]);
    }

    public void firebaseOperations(){


        allNewsList.clear();
        reference= FirebaseDatabase.getInstance().getReference().child("trending_news");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                int index= 0;

               for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                      NewsModes model=new NewsModes();
                      model.setCatId(childSnapshot.child("cat_id").getValue().toString());
                      model.setCategoryName(childSnapshot.child("category_name").getValue().toString());
                      model.setContentType(childSnapshot.child("content_type").getValue().toString());
                      model.setNewsDate(childSnapshot.child("news_date").getValue().toString());
                      model.setNewsDescription(childSnapshot.child("news_description").getValue().toString());
                      model.setNewsImage(childSnapshot.child("news_image").getValue().toString());
                      model.setNewsTitle(childSnapshot.child("news_title").getValue().toString());
                      model.setNewsUrl(childSnapshot.child("news_url").getValue().toString());
                      model.setYoutubeUrl(childSnapshot.child("youtube_url").getValue().toString());

                      try {
                          model.setNewsBy(childSnapshot.child("news_by").getValue().toString());
                      }catch (Exception ex){
                          ex.printStackTrace();
                          model.setNewsBy("NewsZer");

                      }

                      if (model.getNewsImage() !=null && !TextUtils.isEmpty(model.getNewsImage())){
                          allNewsList.add(model);
                      }


                      Log.d(TAG, "onDataChange: "+allNewsList.size());

                      index++;


                    }
                //callTrendingFragment(allNewsList);
                initMainNewsVP(allNewsList);
                contentLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: failed"+databaseError.getMessage());
                contentLayout.setVisibility(View.VISIBLE);

                Snackbar snackbar = Snackbar
                        .make(contentLayout, "Error ! something wrong", Snackbar.LENGTH_LONG)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                firebaseOperations();
                            }
                        });

                snackbar.show();
                Toast.makeText(MainActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void clearLightStatusBar(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = activity.getWindow();
            window.setStatusBarColor(ContextCompat
                    .getColor(activity,R.color.colorPrimaryDark));
        }
    }

    public void firebaseOperations(String catId){
        List<NewsModes> newsList=new ArrayList<>();
        newsList.clear();
        reference= FirebaseDatabase.getInstance().getReference().child("tbl_news");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {

                    if (childSnapshot.child("cat_id").getValue().toString().equals(catId) && childSnapshot.child("news_image").getValue().toString() !=""){
                        NewsModes model=new NewsModes();
                        model.setCatId(childSnapshot.child("cat_id").getValue().toString());
                        model.setCategoryName(childSnapshot.child("category_name").getValue().toString());
                        model.setContentType(childSnapshot.child("content_type").getValue().toString());
                        model.setNewsDate(childSnapshot.child("news_date").getValue().toString());
                        model.setNewsDescription(childSnapshot.child("news_description").getValue().toString());
                        model.setNewsImage(childSnapshot.child("news_image").getValue().toString());
                        model.setNewsTitle(childSnapshot.child("news_title").getValue().toString());
                        model.setNewsUrl(childSnapshot.child("news_url").getValue().toString());
                        model.setYoutubeUrl(childSnapshot.child("youtube_url").getValue().toString());

                        try {
                            model.setNewsBy(childSnapshot.child("news_by").getValue().toString());
                        }catch (Exception ex){
                            ex.printStackTrace();
                            model.setNewsBy("NewsZer");

                        }

                        if (model.getNewsImage() !=null && !TextUtils.isEmpty(model.getNewsImage())){


                        if (!model.getCatId().isEmpty() || model.getCatId().length() !=0 &&
                                !model.getCatId().isEmpty() || model.getCatId().length() !=0 &&
                                !model.getCategoryName().isEmpty() || model.getCategoryName().length() !=0 &&
                                !model.getContentType().isEmpty() || model.getContentType().length() !=0 &&
                                !model.getNewsDate().isEmpty() || model.getNewsDate().length() !=0 &&
                                !model.getNewsDescription().isEmpty() || model.getNewsDescription().length() !=0 &&
                                !model.getNewsImage().isEmpty() || model.getNewsImage().length() !=0 &&
                                !model.getNewsTitle().isEmpty() || model.getNewsTitle().length() !=0 &&
                                !model.getNewsUrl().isEmpty() || model.getNewsUrl().length() !=0){

                              newsList.add(model);

                        }
                    }

                    }

                }

                //callTrendingFragment(DataOperations.sortOrderByCategory(catId,newsList));
                initMainNewsVP(newsList);
                contentLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                contentLayout.setVisibility(View.VISIBLE);

            }
        });
    }





    private void initMainNewsVP(List<NewsModes> newsData){
        if (newsData.size() ==0){
            Snackbar snackbar = Snackbar
                    .make(contentLayout, "Empty ! No news Found", Snackbar.LENGTH_LONG)
                    .setAction("Category", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            drawyerAction();
                        }
                    });

            snackbar.show();
            return;
        }

     /*   NewsModes modes=new NewsModes(false);

        for (int i=0; i<newsData.size(); i++){
            if (i%5==0){
                newsData.add(i,modes);
            }
        }*/

        final NewsPagerAdapter adapter=new NewsPagerAdapter(context,newsData,MainActivity.this);
        FlippableStackView stack = (FlippableStackView)findViewById(R.id.stack);
        //stack.initStack(3);
        stack.initStack(2, StackPageTransformer.Orientation.VERTICAL,0.97f,0.85f,1,StackPageTransformer.Gravity.CENTER);
        stack.setAdapter(adapter);

    }



    private void setWindowFlag(final int bits, boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void drawyerAction(){
        if(drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
           drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            drawerLayout.openDrawer(Gravity.RIGHT);
            drawerLayout.openDrawer(Gravity.RIGHT);
            setFragment(new ListFragment());
        }
    }

    public void anotherDrawerAction(){
        if(drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }
    }

    private void initUiMain(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerNav = (ViewPager) findViewById(R.id.viewpager);

        TextView forYou=(TextView) findViewById(R.id.forYou);
        forYou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               drawyerAction();
               // startActivity(new Intent(getApplicationContext(),TestActivity.class));
            }
        });

        drawerLayout.setScrimColor(Color.TRANSPARENT);
        drawerLayout.requestDisallowInterceptTouchEvent(true);
        drawerLayout.setDrawerElevation(0f);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor =(drawerLayout.getWidth() - (drawerView.getWidth() * slideOffset));
                contentLayout.setTranslationX(-(slideOffset * drawerView.getWidth()));
                //viewPagerNav.setCurrentItem(0);


            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);




        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.list, ListFragment.class)
                .add(R.string.notifications, NotifactionsFragment.class)
                .add(R.string.settings, SettingsFragment.class)
                .create());


        viewPagerNav.setAdapter(adapter);
        viewPagerTab.setViewPager(viewPagerNav);



    }

    public void setKeyTypeMode(){
        MainActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect viewRect = new Rect();
        drawerLayout.getGlobalVisibleRect(viewRect);


        if (!viewRect.contains((int) ev.getRawX(), (int) ev.getRawY())) {

            //hide your navigation view here.

        }
        return super.dispatchTouchEvent(ev);
    }

    public int returnViewPagePosition(){
        return viewPagePosition;
    }

    public void lockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }


    public void unlockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }




    private void initBotomUi(){

        final View bottomSheet =findViewById(R.id.bottom_sheet);

        LinearLayout llBottomSheet = (LinearLayout)findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setHideable(false);
        setFragmentBotom(new CommentFragment());



        final TextView BtmActionTranding=bottomSheet.findViewById(R.id.BtmActionTranding);
        final TextView BtmActionComment=bottomSheet.findViewById(R.id.BtmActionComment);

        BtmActionTranding.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_smaill);
        BtmActionComment.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_white);
        BtmActionTranding.setTextColor(ContextCompat.getColor(context,R.color.black));
        BtmActionComment.setTextColor(ContextCompat.getColor(context,R.color.gray_dark));


        // set callback for changes
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        BtmActionTranding.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_smaill);
                        BtmActionComment.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_white);
                        BtmActionTranding.setTextColor(ContextCompat.getColor(context,R.color.black));
                        BtmActionComment.setTextColor(ContextCompat.getColor(context,R.color.gray_dark));
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        BtmActionComment.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_white);
                        BtmActionTranding.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_white);
                        BtmActionComment.setTextColor(ContextCompat.getColor(context,R.color.black));
                        BtmActionTranding.setTextColor(ContextCompat.getColor(context,R.color.gray_dark));
                        setFragmentBotom(new CommentFragment());
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    bottomSheet.setTranslationY(slideOffset * bottomSheet.getWidth());
                }*/
            }
        });


        BtmActionComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });


        LinearLayout botomBarLayout =findViewById(R.id.botomBarLayout);
        botomBarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });

        BtmActionTranding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtmActionTranding.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_smaill);
                BtmActionComment.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_white);
                BtmActionTranding.setTextColor(ContextCompat.getColor(context,R.color.black));
                BtmActionComment.setTextColor(ContextCompat.getColor(context,R.color.gray_dark));
                //setFragmentBotom(new TrendingFragment());
                if (mBottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else {
                    firebaseOperations();
                }
            }
        });

        BtmActionComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }

                BtmActionComment.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_white);
                BtmActionTranding.setCompoundDrawablesWithIntrinsicBounds(0,0,0, R.drawable.dot_view_android_black_white);
                BtmActionComment.setTextColor(ContextCompat.getColor(context,R.color.black));
                BtmActionTranding.setTextColor(ContextCompat.getColor(context,R.color.gray_dark));
                setFragmentBotom(new CommentFragment());
            }
        });

    }

    public boolean botomSheetIsOpen(){
        if (mBottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED){
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
           return true;
        }else {
            return false;
        }
    }

    private void setFragmentBotom(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.replace(R.id.botomFrame, fragment);
        fragmentTransaction.commit(); // save the changes
    }



    private void setFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.replace(R.id.fragmentView, fragment);
        fragmentTransaction.commit(); // save the changes
    }


    @Override
    public void onSwipeOutAtStart() {
        drawerLayout.getParent().requestDisallowInterceptTouchEvent(true);
        drawyerAction();
    }

    @Override
    public void onSwipeOutAtEnd() {

    }
}