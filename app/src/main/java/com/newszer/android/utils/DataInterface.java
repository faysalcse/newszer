package com.newszer.android.utils;

import com.newszer.android.model.NetModel.NewsModel;

interface DataInterface {
    public boolean result(NewsModel model);
}
