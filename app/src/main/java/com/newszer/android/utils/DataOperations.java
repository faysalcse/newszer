package com.newszer.android.utils;

import android.util.Log;

import com.newszer.android.model.NetModel.Category;
import com.newszer.android.model.NetModel.NewsModel;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataOperations {
    public static final String TAG="MainAction";

    public static List<NewsModel> sortOrderByCategory(String catId, List<NewsModel> data){
        List<NewsModel> models=new ArrayList<>();

       for (NewsModel model : data){
           Log.d(TAG, "sortOrderByCategory: Data size : "+data.size());
           if (model.getCategoryName().equals(catId)){
               models.add(model);
               Log.d(TAG, "sortOrderByCategory: "+model.getNewsTitle());
           }
       }
       return models;
    }

    public static List<Category> deleteNotiificationCategory(List<Category> categories){
        for (Category category : categories){
            if (category.getCategoryImage().equalsIgnoreCase("notification")){
                categories.remove(category);
            }
        }
        return categories;
    }

    public static String sortingUrl(String getUrl){
        try
        {
            URL url = new URL(getUrl);
            String baseUrl = url.getProtocol() + "://" + url.getHost();
            return baseUrl;
        }
        catch (MalformedURLException e)
        {
            return getUrl;
        }
    }

    public String getWebsiteNameFromUrl(String  url){
        try {
            String name=url.replace("https://","");
            return name;
        }catch (Exception ex){
            return url;
        }
    }

    public static String getBaseName(String url){
        Pattern pattern = Pattern.compile("(https?://)([^:^/]*)(:\\d*)?(.*)?");
        Matcher matcher = pattern.matcher(url);

        matcher.find();

        String protocol = matcher.group(1);
        String domain   = matcher.group(2);
        String port     = matcher.group(3);
        String uri      = matcher.group(4);

        return uri;
    }

    public static String getDomainName(String url){
        String urls = null;
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            urls=domain.startsWith("www.") ? domain.substring(4) : domain;
        }catch (Exception e){

        }
        return urls;
    }

    public static String getMonthName(String[] dateArray){

        String mon=dateArray[1].replace("0","");
        int month=Integer.parseInt(mon);
        System.out.println("Month in single char"+month);

        switch (month){
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
        }
        return "";
    }

}
