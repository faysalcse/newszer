package com.newszer.android.utils;

public interface DrawerLocker {
    public void setDrawerEnabled(boolean enabled);
}
