package com.newszer.android.utils;



import com.newszer.android.model.NetModel.Categories;
import com.newszer.android.model.NetModel.Category;

import java.util.ArrayList;
import java.util.List;

public class CallbackCategoryDetails {

    public String status = "";
    public int count = -1;
    public int count_total = -1;
    public int pages = -1;
    public Category category = null;
    public List<Categories> posts = new ArrayList<>();
}
