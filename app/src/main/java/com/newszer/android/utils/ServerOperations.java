package com.newszer.android.utils;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.aapbd.appbajarlib.network.AAPBDHttpClient;
import com.aapbd.appbajarlib.network.NetInfo;
import com.newszer.android.rests.APIObserver;

import java.util.concurrent.Executors;


public class ServerOperations {
    public static final String TAG="ServerOperations";




    public static void callAPI(Context context,String url, final APIObserver apiObserver) {

        final Handler hand = new Handler();

        if (!NetInfo.isOnline(context)) {
            apiObserver.error("No Internet is available.");
            apiObserver.hideProgressBar();
            return;
        }
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String response;
            boolean isError = false;

            @Override
            public void run() {

                try {
                    response = AAPBDHttpClient.get(url).body();

                    Log.e(" save referral  is: ", "" + response);

                } catch (Exception e) {
                    e.printStackTrace();
                    response = e.toString();
                    isError = true;
                }

                hand.post(new Runnable() {
                    @Override
                    public void run() {


                        if (isError) {
                            apiObserver.error(response);
                        } else {
                            apiObserver.completed(response);
                        }

                        apiObserver.hideProgressBar();
                    }
                });


            }
        });
    }

}
