package com.newszer.android;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.model.NetModel.NewsModel;
import com.newszer.android.utils.DataOperations;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.text.TextUtils.isEmpty;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotiHolder> {


    Context context;
    List<NewsModel> data;
    int left_right=1;

    public NotificationAdapter(Context context, List<NewsModel> data) {
        this.context = context;
        this.data = data;
    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @NonNull
    @Override
    public NotiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotiHolder(LayoutInflater.from(context).inflate(R.layout.notifaction_list_items,parent,false));
    }


    @Override
    public void onBindViewHolder(@NonNull NotiHolder holder, int position) {


        NewsModel category=data.get(position);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).drawyerAction();
                if (category.getYoutubeUrl()!=null && category.getYoutubeUrl().length() !=0){
                    context.startActivity(new Intent(context,NewsDetailsViewActivity.class).putExtra("url_news",category.getYoutubeUrl()));
                }else {
                    context.startActivity(new Intent(context,NewsDetailsViewActivity.class).putExtra("url_news",category.getNewsUrl()));
                }
            }
        });



        if (left_right==1){
            left_right=2;
            holder.second_layout.setVisibility(View.GONE);
            holder.first_layout.setVisibility(View.VISIBLE);
            if (category.getNewsDate() !=null || !isEmpty(category.getNewsDate())){
                String date=category.getNewsDate().split("\\s")[0];
                String[] dateArray=date.split("-");
                holder.dateOFnews.setText(dateArray[2]+" "+DataOperations.getMonthName(dateArray)+" "+dateArray[0]);
            }
            if (category.getCategoryName() !=null || !isEmpty(category.getCategoryName())){
                holder.catNameOfNews.setText(category.getCategoryName());
            }


            if (category.getNewsDescription() !=null || !isEmpty(category.getNewsDescription())){
                holder.detailsOfNews.setText(Html.fromHtml(category.getNewsDescription()));
            }




            if (category.getNewsImage() !=null || !isEmpty(category.getNewsImage())){

                try {

                    Picasso.get()
                            .load(category.getNewsImage())
                            .placeholder(R.drawable.loading_placeholder)
                            .error(R.drawable.ic_error)
                            .into(holder.notiThumbnial);
                }catch (Exception e){
                    holder.notiThumbnial.setImageResource(R.drawable.ic_error);
                }
            }
        }else {
            left_right=1;
            holder.first_layout.setVisibility(View.GONE);
            holder.second_layout.setVisibility(View.VISIBLE);
            if (category.getNewsDate() !=null || !isEmpty(category.getNewsDate())){

               String date=category.getNewsDate().split("\\s")[0];
               String[] dateArray=date.split("-");

                holder.two_dateOFnews.setText(dateArray[2]+" "+DataOperations.getMonthName(dateArray)+" "+dateArray[0]);
            }
            if (category.getCategoryName() !=null || !isEmpty(category.getCategoryName())){
                holder.two_catNameOfNews.setText(category.getCategoryName());
            }


            if (category.getNewsDescription() !=null || !isEmpty(category.getNewsDescription())){
                holder.two_detailsOfNews.setText(Html.fromHtml(category.getNewsDescription()));
            }


            if (category.getNewsImage() !=null || !isEmpty(category.getNewsImage())){
                Picasso.get()
                        .load(category.getNewsImage())
                        .placeholder(R.drawable.loading_placeholder)
                        .error(R.drawable.ic_error)
                        .into(holder.two_notiThumbnial);
            }



        }






    }



    class NotiHolder extends RecyclerView.ViewHolder {

        ImageView notiThumbnial;
        TextView dateOFnews;
        TextView catNameOfNews;
        LinearLayout first_layout;
        LinearLayout second_layout;
        ImageView two_notiThumbnial;
        TextView two_dateOFnews;
        TextView two_catNameOfNews;
        TextView detailsOfNews;
        TextView two_detailsOfNews;

        View view;


        public NotiHolder(@NonNull View itemView) {
            super(itemView);
            notiThumbnial=itemView.findViewById(R.id.notiThumbnial);
            dateOFnews=itemView.findViewById(R.id.dateOFnews);
            catNameOfNews=itemView.findViewById(R.id.catNameOfNews);
            detailsOfNews=itemView.findViewById(R.id.detailsOfNews);

            two_notiThumbnial=itemView.findViewById(R.id.two_notiThumbnial);
            two_dateOFnews=itemView.findViewById(R.id.two_dateOFnews);
            two_catNameOfNews=itemView.findViewById(R.id.two_catNameOfNews);
            two_detailsOfNews=itemView.findViewById(R.id.two_detailsOfNews);

            first_layout=itemView.findViewById(R.id.first_layout);
            second_layout=itemView.findViewById(R.id.second_layout);
            view=itemView;
        }
    }
}


