package com.newszer.android.rests;

public interface APIObserver {
    void completed(Object data);

    void error(Object error);

    void hideProgressBar();
}
