package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.newszer.R;
import com.newszer.android.constant.Constants;
import com.newszer.android.model.Comments;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

public class CommentAdapter extends BaseAdapter {

    Context con;
    LayoutInflater inflater;
    List<Comments> categories;

    public CommentAdapter(Context con, List<Comments> categories) {
        this.con = con;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    class MyHolder{
        TextView tv_userName;
        TextView tv_comment;
        TextView likeCount;
        TextView CommentCount;
        ImageView userImage;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MyHolder holder=new MyHolder();
        inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(R.layout.list_item_comment,viewGroup,false);

        holder.tv_userName=(TextView)view.findViewById(R.id.tv_userName);
        holder.likeCount=(TextView)view.findViewById(R.id.likeCount);
        holder.CommentCount=(TextView)view.findViewById(R.id.CommentCount);
        holder.tv_comment=(TextView)view.findViewById(R.id.tv_comment);
        holder.userImage=(ImageView) view.findViewById(R.id.userImage);

        holder.tv_userName.setText(categories.get(i).getUserName());
        holder.tv_comment.setText(categories.get(i).getCommentMsg());

        String rand=null;


        Random random=new Random();
        int randomComment = random.nextInt(50);
        int randomCLike= random.nextInt(50);

        holder.tv_userName.setText(randomComment);
        holder.tv_userName.setText(randomCLike);

        Picasso.get()
               /* .load(Constants.Api.CATEGORIES_IMAGE+categories.get(i).getCommentsImage())*/
                .load(Constants.Api.EXAMPLE_IMG)
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .into(holder.userImage);


        return view;
    }

    public String getCatId(int pos){
        return String.valueOf(categories.get(pos).getUserName());
    }
}
