package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TrendingAdpers extends RecyclerView.Adapter<ImageHolder> {
    Context context;
    List<String> imagList;

    public TrendingAdpers (Context context, List<String> imagList) {
        this.context = context;
        this.imagList = imagList;

    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(LayoutInflater.from(context).inflate(R.layout.list_items,parent,false));
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {

        Picasso.get()
                .load(imagList.get(position))
                .placeholder(R.drawable.loading_placeholder)
                .error(R.drawable.ic_error)
                .into(holder.contentImag);
    }

    @Override
    public int getItemCount() {
        return imagList.size();
    }

}
