package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newszer.android.MainActivity;
import com.newszer.R;
import com.newszer.android.model.NetModel.Category;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

public class CategoryViewAdapter extends RecyclerView.Adapter<CategoryViewAdapter.CategoryViewHolder>{

    Context con;
    List<Category> categories;

    public CategoryViewAdapter(Context con, List<Category> categories) {
        this.con = con;
        this.categories = categories;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(con).inflate(R.layout.category_list,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {

        Category category=categories.get(position);

        if(category.getCategoryName() != null && !category.getCategoryName().isEmpty()){
            holder.title.setText(category.getCategoryName());
        }

        Random random=new Random();
        holder.totalRead.setText(+random.nextInt(50)+"m reading about this");

        if(category.getCategoryImage() != null && !category.getCategoryImage().isEmpty()){
            Picasso.get().load(category.getCategoryImage()).placeholder(R.drawable.loading_placeholder).fit().into(holder.catImage);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)con).drawyerAction();
                ((MainActivity)con).firebaseOperations(category.getCid());
            }
        });


    }

    @Override
    public int getItemCount() {
        return categories.size();
    }


    class CategoryViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView totalRead;
        ImageView catImage;

        View view;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            view=itemView;
            title=(TextView)itemView.findViewById(R.id.categoryName);
            totalRead=(TextView)itemView.findViewById(R.id.reading_about);
            catImage=(ImageView) itemView.findViewById(R.id.category_image);


        }
    }
}
