package com.newszer.android.adapter;



import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.newszer.android.NewsDetailsViewActivity;
import com.newszer.R;
import com.newszer.android.constant.Constants;
import com.newszer.android.model.NetModel.NewsModel;

import java.util.List;

 public class TrandingNewsAdapter extends RecyclerView.Adapter<TrandingNewsAdapter.TrandingViewHolder> {


    Context context;
    List<NewsModel> data;
    View view;
    String newsUrl;

    public TrandingNewsAdapter(Context context, List<NewsModel> data) {
        this.context = context;
        this.data = data;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @NonNull
    @Override
    public TrandingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view=LayoutInflater.from(context).inflate(R.layout.trending_news_list_items, parent, false);
        return new TrandingViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull TrandingViewHolder holder, int position) {




        NewsModel newsModel = data.get(position);



        if (newsModel.getNewsTitle() != null || !TextUtils.isEmpty(newsModel.getNewsTitle())) {
            holder.newsTitle.setText(newsModel.getNewsTitle());
        }
        if (newsModel.getCategoryName() != null || !TextUtils.isEmpty(newsModel.getCategoryName())) {
            holder.newsCategory.setText(newsModel.getCategoryName());
        }




        if (newsModel.getNewsImage() != null || !TextUtils.isEmpty(newsModel.getNewsImage())) {
            /*Picasso.get()
                    .load(Constants.Api.NEWS_IMAGE+newsModel.getNewsImage())
                    .placeholder(R.drawable.loading_placeholder)
                    .error(R.drawable.example_imgs)
                    .into(holder.newsThumbnail);*/

            Glide.with(context).load(Constants.Api.NEWS_IMAGE+newsModel.getNewsImage())
                    .thumbnail(Glide.with(context).load(R.drawable.loading_placeholder))
                    .fitCenter()
                    .into(holder.newsThumbnail);
        }

        view.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                if (newsModel.getNewsUrl() != null && !newsModel.getNewsUrl().isEmpty()){
                    newsUrl=newsModel.getNewsUrl();
                }else {
                    newsUrl=newsModel.getYoutubeUrl();
                }

                context.startActivity(new Intent(context, NewsDetailsViewActivity.class).putExtra("url_news",newsUrl));
            }
        });
    }

  class TrandingViewHolder  extends RecyclerView.ViewHolder {

         ImageView newsThumbnail;
         TextView newsTitle;
         TextView newsCategory;


         public TrandingViewHolder(@NonNull View itemView) {
             super(itemView);
             newsThumbnail=itemView.findViewById(R.id.newsThumbnail);
             newsTitle=itemView.findViewById(R.id.newsTitle);
             newsCategory=itemView.findViewById(R.id.newsCategory);
         }
     }
 }



