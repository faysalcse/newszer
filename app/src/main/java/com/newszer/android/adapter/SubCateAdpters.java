package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.model.SubCate;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SubCateAdpters extends RecyclerView.Adapter<SubCategoryHolder> {
    Context context;
    List<SubCate> subCateData;

    public SubCateAdpters(Context context, List<SubCate> subCateData) {
        this.context = context;
        this.subCateData = subCateData;

    }

    @Override
    public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SubCategoryHolder(LayoutInflater.from(context).inflate(R.layout.sub_category_items,parent,false));
    }

    @Override
    public void onBindViewHolder(SubCategoryHolder holder, int position) {

        holder.subCategoryName.setText(subCateData.get(position).getTitle());

        Picasso.get()
                .load(subCateData.get(position).getImgUrl())
                .placeholder(R.drawable.loading_placeholder)
                .error(R.drawable.ic_error)
                .into(holder.categoryImage);


    }

    @Override
    public int getItemCount() {
        return subCateData.size();
    }

}
