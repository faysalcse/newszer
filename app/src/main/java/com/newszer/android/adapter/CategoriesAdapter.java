package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.model.NetModel.Category;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder>  {


    Context con;
    List<Category> categories;

    public CategoriesAdapter(Context con, List<Category> categories) {
        this.con = con;
        this.categories = categories;
    }


    @NonNull
    @Override
    public CategoriesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(con).inflate(R.layout.category_list, parent, false);
        return new CategoriesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesHolder holder, int position) {

        holder.title.setText(categories.get(position).getCategoryName());
        Random random=new Random();
        holder.totalRead.setText(+random.nextInt(50)+"m reading about this");

        Picasso.get().load(categories.get(position).getCategoryImage())
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .into(holder.catImage);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class CategoriesHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView totalRead;
        ImageView catImage;
        public CategoriesHolder(@NonNull View view) {
            super(view);

            title=(TextView)view.findViewById(R.id.categoryName);
            totalRead=(TextView)view.findViewById(R.id.reading_about);
            catImage=(ImageView) view.findViewById(R.id.category_image);

        }
    }
}
