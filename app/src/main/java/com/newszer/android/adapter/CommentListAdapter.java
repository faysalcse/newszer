package com.newszer.android.adapter;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.model.Comments;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.CommentsHolder> {


   Context context;
   List<Comments> data;
   View view;
   String newsUrl;

   public CommentListAdapter(Context context, List<Comments> data) {
       this.context = context;
       this.data = data;
   }


   @Override
   public int getItemCount() {
       return data.size();
   }

   @NonNull
   @Override
   public CommentsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       view=LayoutInflater.from(context).inflate(R.layout.list_item_comment, parent, false);
       return new CommentsHolder(view);
   }


   @Override
   public void onBindViewHolder(@NonNull CommentsHolder holder, int position) {
       Comments newsModel = data.get(position);

       holder.tv_userName.setText(newsModel.getUserName());
       holder.tv_comment.setText(newsModel.getCommentMsg());

       Random random=new Random();


       holder.likeCount.setText(newsModel.getLikes()+" Likes");
       holder.hourView.setText(newsModel.getHour()+"h.");
       holder.CommentCount.setText(newsModel.getReplay()+" Replay");

       Picasso.get()
                .load(newsModel.getUserImage())
               .placeholder(R.drawable.loading_placeholder)
               .fit()
               .into(holder.userImage);




   }

 class CommentsHolder  extends RecyclerView.ViewHolder {

     TextView tv_userName;
     TextView tv_comment;
     TextView hourView;
     TextView likeCount;
     TextView CommentCount;
     ImageView userImage;


        public CommentsHolder(@NonNull View itemView) {
            super(itemView);
            tv_userName=(TextView)itemView.findViewById(R.id.tv_userName);
            tv_comment=(TextView)itemView.findViewById(R.id.tv_comment);
            hourView=(TextView)itemView.findViewById(R.id.hourView);
            likeCount=(TextView)itemView.findViewById(R.id.likeCount);
            CommentCount=(TextView)itemView.findViewById(R.id.CommentCount);
            userImage=(ImageView) itemView.findViewById(R.id.userImage);
        }
    }
}



