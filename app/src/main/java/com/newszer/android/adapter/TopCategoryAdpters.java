package com.newszer.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.model.NetModel.Category;

import java.util.List;

public class TopCategoryAdpters extends RecyclerView.Adapter<TopCategoryHolder> {
    Context context;
    List<Category> subCateData;
    OnItemClickListener listener;
    int pos=999;

    public TopCategoryAdpters(Context context, List<Category> subCateData,OnItemClickListener listener) {
        this.context = context;
        this.subCateData = subCateData;
        this.listener = listener;

    }
    public interface OnItemClickListener {
        void onItemClick(Category item);
    }

    public int getIdOfPosition(int position){
        return Integer.parseInt(subCateData.get(position).getCid().toString());
    }

    @Override
    public TopCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TopCategoryHolder(LayoutInflater.from(context).inflate(R.layout.top_category_item,parent,false));
    }

    @Override
    public void onBindViewHolder(TopCategoryHolder holder, int position) {
        holder.bind(subCateData.get(position), listener);
        holder.categoryName.setText(subCateData.get(position).getCategoryName());
       // holder.categoryName.setTextSize(20);

        Category category=subCateData.get(position);

        if(category.isTextChanged()){
            holder.categoryName.setTextColor(Color.BLACK);
           // holder.categoryName.setTextSize(22);
        }else{
            holder.categoryName.setTextColor(ContextCompat.getColor(context,R.color.colorGreyDark));
            //holder.categoryName.setTextSize(16);
        }

        holder.categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos !=999){
                    changeTextNormal(pos);
                }
                changeTextBold(position);
                pos=position;
            }
        });


    }

    public void changeTextBold(int index) {
        subCateData.get(index).setTextChanged(true);
        notifyItemChanged(index);

    }
    public void changeTextNormal(int index) {
        subCateData.get(index).setTextChanged(false);
        notifyItemChanged(index);

    }

    @Override
    public int getItemCount() {
        return subCateData.size();
    }

}
