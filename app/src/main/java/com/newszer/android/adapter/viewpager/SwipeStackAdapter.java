package com.newszer.android.adapter.viewpager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.newszer.R;
import com.newszer.android.model.News;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SwipeStackAdapter extends BaseAdapter {

    private List<News> mData;
    LayoutInflater inflater;
    Context context;

    public SwipeStackAdapter(List<News> data,Context context) {
        this.mData = data;
        this.context=context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position).getThumbnail();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        inflater=LayoutInflater.from(context);
        view =inflater.inflate(R.layout.news_mainview_item, parent, false);

        ImageView thumbnailOfNews=(android.widget.ImageView)view.findViewById(R.id.thumbnailImage);
        TextView titleOfNews=(TextView) view.findViewById(R.id.newsTitle);
        TextView detailsOfNews=(TextView) view.findViewById(R.id.newsDetails);
        TextView categoryOfNews=(TextView) view.findViewById(R.id.newsCategory);

        final News pager=mData.get(position);


        if (pager.getNewsTitle() !=null || pager.getNewsTitle() !=""){
            titleOfNews.setText(pager.getNewsTitle());
        }
        if (pager.getNewsDetails() !=null || pager.getNewsDetails() !=""){
            detailsOfNews.setText(pager.getNewsDetails());
        }

        if (pager.getCategory() !=null || pager.getCategory() !=""){
            categoryOfNews.setText(pager.getCategory());
        }

        if (pager.getThumbnail() !=null || pager.getThumbnail() !=""){
            Picasso.get()
                    .load(pager.getThumbnail())
                    .placeholder(R.drawable.loading_placeholder)
                    .error(R.drawable.ic_error)
                    .into(thumbnailOfNews);
        }


        return view;
    }
}