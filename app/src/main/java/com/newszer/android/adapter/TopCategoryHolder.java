package com.newszer.android.adapter;


import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.model.NetModel.Category;

class TopCategoryHolder extends RecyclerView.ViewHolder {

    TextView categoryName;
    public TopCategoryHolder(View itemView) {
        super(itemView);
        categoryName=(TextView)itemView.findViewById(R.id.categoryName);
    }

    public void bind(final Category item, final TopCategoryAdpters.OnItemClickListener listener) {

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(item);
            }
        });
    }


}