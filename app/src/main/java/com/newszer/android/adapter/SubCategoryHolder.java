package com.newszer.android.adapter;


import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.makeramen.roundedimageview.RoundedImageView;

class SubCategoryHolder extends RecyclerView.ViewHolder{

    RoundedImageView categoryImage;
    TextView subCategoryName;
    public SubCategoryHolder(View itemView) {
        super(itemView);
        categoryImage=(RoundedImageView)itemView.findViewById(R.id.sub_cat_image);
        subCategoryName=(TextView)itemView.findViewById(R.id.subCategoryName);
    }
}