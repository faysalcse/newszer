package com.newszer.android.adapter;


import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;

class ImageHolder extends RecyclerView.ViewHolder{

    ImageView contentImag;
    public ImageHolder(View itemView) {
        super(itemView);
        contentImag=(ImageView)itemView.findViewById(R.id.wallImage);
    }
}