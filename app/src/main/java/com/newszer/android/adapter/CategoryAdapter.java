package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.newszer.R;
import com.newszer.android.model.NetModel.Category;

import java.util.List;
import java.util.Random;

public class CategoryAdapter extends BaseAdapter {

    Context con;
    LayoutInflater inflater;
    List<Category> categories;

    public CategoryAdapter(Context con, List<Category> categories) {
        this.con = con;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    class MyHolder{
        TextView title;
        TextView totalRead;
        ImageView catImage;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MyHolder holder=new MyHolder();
        inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(R.layout.category_list,viewGroup,false);

        holder.title=(TextView)view.findViewById(R.id.categoryName);
        holder.totalRead=(TextView)view.findViewById(R.id.reading_about);
        holder.catImage=(ImageView) view.findViewById(R.id.category_image);

        holder.title.setText(categories.get(i).getCategoryName());
        Random random=new Random();
        holder.totalRead.setText(+random.nextInt(50)+"m reading about this");
/*
        Picasso.get()
               *//* .load(Constants.Api.CATEGORIES_IMAGE+categories.get(i).getCategoryImage())*//*
                .load(categories.get(i).getCategoryImage())
                *//*.load(categories.get(i).getCategoryImage())*//*
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .into(holder.catImage);*/


        return view;
    }

    public String getCatId(int pos){
        return String.valueOf(categories.get(pos).getCid());
    }
}
