package com.newszer.android.adapter.viewpager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.newszer.android.MainActivity;
import com.newszer.android.NewsDetailsViewActivity;
import com.newszer.R;
import com.newszer.android.constant.Constants;
import com.newszer.android.model.NetModel.NewsModel;
import com.newszer.android.model.NewsModes;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NewsPagerAdapter extends PagerAdapter{

    List<NewsModes> newSource;
    Context con;
    LayoutInflater layoutInflater;
    Activity activity;


    private ViewGroup nativeAdLayout;
    private LinearLayout adView;
    private NativeAd nativeAd;


    public NewsPagerAdapter(Context con, List<NewsModes> newSource,Activity activity){
        this.con=con;
        this.newSource=newSource;
        this.activity=activity;
    }



    @Override
    public int getCount() {
        return newSource.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view==(FrameLayout)object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((FrameLayout)object);
    }



    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        if (position %7==0){
            Log.d("34534534gfdgfdfg", "instantiateItem: if"+position);
            layoutInflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view=layoutInflater.inflate(R.layout.native_ads_item,container,false);
            loadNativeAd(view);
            container.addView(view);

            return view;
        }else {
            Log.d("34534534gfdgfdfg", "instantiateItem: else "+position);
            layoutInflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.news_mainview_item, container, false);

            NewsModes model=newSource.get(position);

            contentLayoutView(view,model,position);


            container.addView(view);
            return view;
        }
    }

    private void contentLayoutView(View view,NewsModes pager ,int position) {
        ImageView thumbnailOfNews = (ImageView) view.findViewById(R.id.thumbnailImage);
        TextView titleOfNews = (TextView) view.findViewById(R.id.newsTitle);
        TextView detailsOfNews = (TextView) view.findViewById(R.id.newsDetails);
        TextView categoryOfNews = (TextView) view.findViewById(R.id.newsCategory);
        TextView newsAnotherTitle = (TextView) view.findViewById(R.id.newsAnotherTitle);
        TextView newsAnotherCategory = (TextView) view.findViewById(R.id.newsAnotherCategory);
        TextView newsOfWebsite = (TextView) view.findViewById(R.id.newsOfWebsite);
        TextView postedTime = (TextView) view.findViewById(R.id.postedTime);

        LinearLayout newsLayout = (LinearLayout) view.findViewById(R.id.newsLayout);
        LinearLayout videoLayout = (LinearLayout) view.findViewById(R.id.videoLayout);

        CardView loadingCard = (CardView) view.findViewById(R.id.loadingCard);
        CardView mainCard = (CardView) view.findViewById(R.id.mainCard);
        ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmerFrame);
        shimmerFrameLayout.startShimmer();


        mainCard.setVisibility(View.GONE);
        loadingCard.setVisibility(View.VISIBLE);

        String newsUrl;


        if (pager.getNewsDescription() != null && pager.getNewsDescription() != "") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                detailsOfNews.setText(Html.fromHtml(pager.getNewsDescription(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                detailsOfNews.setText(Html.fromHtml(pager.getNewsDescription()));
            }
        }


        if (pager.getYoutubeUrl() != null && pager.getYoutubeUrl().length() != 0) {
            newsLayout.setVisibility(View.GONE);
            videoLayout.setVisibility(View.VISIBLE);

            newsUrl = pager.getYoutubeUrl();

            if (pager.getNewsTitle() != null && pager.getNewsTitle() != "") {
                newsAnotherTitle.setText((pager.getNewsTitle()).replace("\\\"", "\""));
            }
            if (pager.getNewsDescription() != null && pager.getNewsDescription() != "") {
                newsAnotherCategory.setText(pager.getCategoryName());
            }
        } else {
            newsLayout.setVisibility(View.VISIBLE);
            videoLayout.setVisibility(View.GONE);

            newsUrl = pager.getNewsUrl();

            if (pager.getNewsTitle() != null && !TextUtils.isEmpty(pager.getNewsTitle())) {
                titleOfNews.setText((pager.getNewsTitle()).replace("\\\"", "\"").replace("\\\'", "\'"));
                //  titleOfNews.setText(pager.getNewsTitle());
            }

            if (pager.getCategoryName() != null && !TextUtils.isEmpty(pager.getCategoryName())) {
                categoryOfNews.setText(pager.getCategoryName());
            }
        }


        if (pager.getNewsBy() != null && !TextUtils.isEmpty(pager.getNewsBy())) {
            newsOfWebsite.setText(pager.getNewsBy());
        }

        if (pager.getNewsDate() != null && !TextUtils.isEmpty(pager.getNewsDate())) {
            postedTime.setText(pager.getNewsDate());
        }


        if (pager.getNewsImage() != null && !TextUtils.isEmpty(pager.getNewsImage())) {

            try {
                Picasso.get()
                        .load(pager.getNewsImage())
                        .placeholder(R.drawable.loading_placeholder)
                        .error(R.drawable.ic_error_image)
                        .into(thumbnailOfNews, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                mainCard.setVisibility(View.VISIBLE);
                                loadingCard.setVisibility(View.GONE);
                                shimmerFrameLayout.stopShimmer();
                            }

                            @Override
                            public void onError(Exception e) {

                            }

                        });

            }catch (Exception e){
                Log.d("fdsgs" ,"instantiateItem: "+e.getMessage());
            }

        }



        view.setOnTouchListener(new OnSwipeTouchListener(con) {
            public void onSwipeTop() {
            }

            public void onSwipeRight() {
                ((MainActivity) con).anotherDrawerAction();
            }

            public void onSwipeLeft() {
                ((MainActivity) con).drawyerAction();
            }

            public void onSwipeBottom() {
            }

            public void onSingleTap() {

                boolean isExpand = ((MainActivity) con).botomSheetIsOpen();

                if (!isExpand) {
                    con.startActivity(new Intent(con, NewsDetailsViewActivity.class).putExtra("url_news", newsUrl));
                }

            }


        });
    }

    private void loadNativeAd(View view) {
        // Instantiate a NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = new NativeAd(con, con.getString(R.string.nativeads_placement_id));

        nativeAd.setAdListener(new NativeAdListener() {

            @Override
            public void onMediaDownloaded(Ad ad) {

            }

            @Override
            public void onError(Ad ad, AdError adError) {

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                inflateAd(nativeAd,view);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

        });

        // Request an ad
        nativeAd.loadAd();
    }

    private void inflateAd(NativeAd nativeAd,View view) {

        nativeAd.unregisterView();

        // Add the Ad view into the ad container.
        nativeAdLayout = view.findViewById(R.id.native_ad_container);
        LayoutInflater inflater = LayoutInflater.from(con);
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView = (LinearLayout) inflater.inflate(R.layout.native_ad_layout, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer =view.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(con, nativeAd, (NativeAdLayout) nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
    }


}
