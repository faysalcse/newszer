package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newszer.R;

import java.util.List;

public class SkillsAdapter extends BaseAdapter {

    List<String> skillsList;
    Context context;
    LayoutInflater inflater;

    public SkillsAdapter(Context context,List<String> skillsList) {
        this.skillsList = skillsList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return skillsList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        inflater=LayoutInflater.from(context);
        view=inflater.inflate(R.layout.skil_item_layout,viewGroup,false);
        TextView skillsTitle=(TextView)view.findViewById(R.id.skillsTitle);
        if (skillsList.get(i)!=null || skillsList.get(i) !=""){
            if (skillsList.get(i).equalsIgnoreCase("add")){
                skillsTitle.setText("Add Skill");
                skillsTitle.setBackgroundResource(R.drawable.add_skill_background);
            }else {
                skillsTitle.setText(skillsList.get(i));
            }
        }
        return view;
    }
}
