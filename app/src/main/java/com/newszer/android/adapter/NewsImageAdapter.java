package com.newszer.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;
import com.newszer.android.model.Places;
import com.makeramen.roundedimageview.RoundedImageView;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

public class NewsImageAdapter extends RecyclerView.Adapter<NewsImageHolder> {

    public static final String TAG="NewsImageAdapter__";

    Context con;
    List<Places> newsImageData;

    public NewsImageAdapter(Context con, List<Places> newsImageData) {
        this.newsImageData = newsImageData;
        this.con = con;
    }

    @Override
    public NewsImageHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(con).inflate(R.layout.image_news_list_items,viewGroup,false);
        return new NewsImageHolder(view);
    }

    @Override
    public void onBindViewHolder(final NewsImageHolder holder, final int i) {


        if (newsImageData.get(i).getContent() != null && newsImageData.get(i).getContent() != ""){
            holder.imageContent.setText(newsImageData.get(i).getContent());
         }

        if (newsImageData.get(i).getCity() != null && newsImageData.get(i).getCity() != ""){
            holder.city.setText(newsImageData.get(i).getCity());
        }

        if (newsImageData.get(i).getCountry() != null && newsImageData.get(i).getCountry() != ""){
            holder.country.setText(newsImageData.get(i).getCountry());
        }

        if (newsImageData.get(i).getImageUrl() != null && newsImageData.get(i).getImageUrl() != ""){
            if (i==0){
                ViewGroup.LayoutParams layoutParams = holder.mainLayout.getLayoutParams();
                layoutParams.width = 380;
                holder.mainLayout.setLayoutParams(layoutParams);
            }
            setNewsImage(holder,i);
        }




    }

    private void setNewsImage(NewsImageHolder holder,int pos){
        RoundedImageView riv = new RoundedImageView(con);
        Transformation transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .build();

        Picasso.get().load(newsImageData.get(pos).getImageUrl())
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .transform(transformation)
                .into(holder.wallImage);



    }

    @Override
    public int getItemCount() {
        return newsImageData.size();
    }

    public void filterList(ArrayList<Places> filteredList) {
        newsImageData = filteredList;
        notifyDataSetChanged();
    }


}


