package com.newszer.android.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.newszer.R;


public class NewsImageHolder extends RecyclerView.ViewHolder {

    ImageView wallImage;
    TextView imageContent;
    TextView city;
    TextView country;
    LinearLayout mainLayout;



    public NewsImageHolder(View itemView) {
        super(itemView);
        imageContent=(TextView)itemView.findViewById(R.id.imageOverlepContent);
        city=(TextView)itemView.findViewById(R.id.cityName);
        country=(TextView)itemView.findViewById(R.id.countryName);
        wallImage=(ImageView) itemView.findViewById(R.id.wallImage);
        mainLayout=(LinearLayout)itemView.findViewById(R.id.mainLayout);
    }
}
