package com.newszer.android.model.NetModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsModel {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("news_date")
    @Expose
    private String newsDate;
    @SerializedName("news_description")
    @Expose
    private String newsDescription;
    @SerializedName("news_image")
    @Expose
    private String newsImage;
    @SerializedName("news_title")
    @Expose
    private String newsTitle;
    @SerializedName("news_url")
    @Expose
    private String newsUrl;
    @SerializedName("youtube_url")
    @Expose
    private String youtubeUrl;

    /**
     * No args constructor for use in serialization
     *
     */
    public NewsModel() {
    }

    /**
     *
     * @param youtubeUrl
     * @param newsImage
     * @param categoryName
     * @param newsUrl
     * @param newsDescription
     * @param newsDate
     * @param catId
     * @param contentType
     * @param newsTitle
     */
    public NewsModel(String catId, String categoryName, String contentType, String newsDate, String newsDescription, String newsImage, String newsTitle, String newsUrl, String youtubeUrl) {
        super();
        this.catId = catId;
        this.categoryName = categoryName;
        this.contentType = contentType;
        this.newsDate = newsDate;
        this.newsDescription = newsDescription;
        this.newsImage = newsImage;
        this.newsTitle = newsTitle;
        this.newsUrl = newsUrl;
        this.youtubeUrl = youtubeUrl;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

    public String getNewsDescription() {
        return newsDescription;
    }

    public void setNewsDescription(String newsDescription) {
        this.newsDescription = newsDescription;
    }

    public String getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(String newsImage) {
        this.newsImage = newsImage;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    @Override
    public String toString() {
        return "NewsModel{" +
                "catId='" + catId + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", contentType='" + contentType + '\'' +
                ", newsDate='" + newsDate + '\'' +
                ", newsDescription='" + newsDescription + '\'' +
                ", newsImage='" + newsImage + '\'' +
                ", newsTitle='" + newsTitle + '\'' +
                ", newsUrl='" + newsUrl + '\'' +
                ", youtubeUrl='" + youtubeUrl + '\'' +
                '}';
    }
}