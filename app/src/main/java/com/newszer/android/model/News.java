package com.newszer.android.model;

public class News {
    String newsTitle;
    String newsDetails;
    String category;
    String thumbnail;
    String postDate;
    String contentWriter;


    public News() {
    }

    public News(String newsTitle, String newsDetails, String category, String thumbnail) {
        this.newsTitle = newsTitle;
        this.newsDetails = newsDetails;
        this.category = category;
        this.thumbnail = thumbnail;
    }

    public News( String category,String newsTitle, String thumbnail) {
        this.newsTitle = newsTitle;
        this.category = category;
        this.thumbnail = thumbnail;
    }

    public News(String newsTitle, String newsDetails, String category, String thumbnail, String postDate, String contentWriter) {
        this.newsTitle = newsTitle;
        this.newsDetails = newsDetails;
        this.category = category;
        this.thumbnail = thumbnail;
        this.postDate = postDate;
        this.contentWriter = contentWriter;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsDetails() {
        return newsDetails;
    }

    public void setNewsDetails(String newsDetails) {
        this.newsDetails = newsDetails;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getContentWriter() {
        return contentWriter;
    }

    public void setContentWriter(String contentWriter) {
        this.contentWriter = contentWriter;
    }
}
