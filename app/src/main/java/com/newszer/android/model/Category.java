package com.newszer.android.model;

public class Category {
    String title;
    String totalRead;
    String catImage;

    public Category(String title, String totalRead, String catImage) {
        this.title = title;
        this.totalRead = totalRead;
        this.catImage = catImage;
    }

    public Category(String title, String totalRead) {
        this.title = title;
        this.totalRead = totalRead;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalRead() {
        return totalRead;
    }

    public void setTotalRead(String totalRead) {
        this.totalRead = totalRead;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }
}
