
package com.newszer.android.model.NetModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("post_count")
    @Expose
    private String postCount;


    public boolean isTextChanged;

    public Category() {
    }

    public Category(String cid, String categoryName, String categoryImage) {
        this.cid = cid;
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setTextChanged(boolean imageChanged) {
        isTextChanged = imageChanged;
    }

    public boolean isTextChanged() {
        return isTextChanged;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getPostCount() {
        return postCount;
    }

    public void setPostCount(String postCount) {
        this.postCount = postCount;
    }

}
