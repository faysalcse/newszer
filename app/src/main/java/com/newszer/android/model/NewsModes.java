package com.newszer.android.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsModes {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("news_by")
    @Expose
    private String newsBy;
    @SerializedName("news_date")
    @Expose
    private String newsDate;
    @SerializedName("news_description")
    @Expose
    private String newsDescription;
    @SerializedName("news_image")
    @Expose
    private String newsImage;
    @SerializedName("news_title")
    @Expose
    private String newsTitle;
    @SerializedName("news_url")
    @Expose
    private String newsUrl;
    @SerializedName("youtube_url")
    @Expose
    private String youtubeUrl;

    private boolean ads;

    public NewsModes(boolean ads) {
        this.ads = ads;
    }



    /**
     * No args constructor for use in serialization
     *
     */
    public NewsModes() {
    }

    /**
     *
     * @param youtubeUrl
     * @param newsImage
     * @param categoryName
     * @param newsUrl
     * @param newsBy
     * @param newsDescription
     * @param newsDate
     * @param catId
     * @param contentType
     * @param newsTitle
     */
    public NewsModes(String catId, String categoryName, String contentType, String newsBy, String newsDate, String newsDescription, String newsImage, String newsTitle, String newsUrl, String youtubeUrl) {
        super();
        this.catId = catId;
        this.categoryName = categoryName;
        this.contentType = contentType;
        this.newsBy = newsBy;
        this.newsDate = newsDate;
        this.newsDescription = newsDescription;
        this.newsImage = newsImage;
        this.newsTitle = newsTitle;
        this.newsUrl = newsUrl;
        this.youtubeUrl = youtubeUrl;
    }

    public boolean getAds() {
        return ads;
    }

    public void setAds(boolean ads) {
        this.ads = ads;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getNewsBy() {
        return newsBy;
    }

    public void setNewsBy(String newsBy) {
        this.newsBy = newsBy;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

    public String getNewsDescription() {
        return newsDescription;
    }

    public void setNewsDescription(String newsDescription) {
        this.newsDescription = newsDescription;
    }

    public String getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(String newsImage) {
        this.newsImage = newsImage;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }


}