package com.newszer.android.model;

public class Places {
    String content;
    String title;
    String ImageUrl;
    String city;
    String Country;

    public Places(String content, String title, String imageUrl, String city, String country) {
        this.content = content;
        this.title = title;
        ImageUrl = imageUrl;
        this.city = city;
        Country = country;
    }

    public Places(String title, String content, String imageId) {
        this.content = content;
        this.title = title;
        ImageUrl = imageId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageId) {
        ImageUrl = imageId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }
}
