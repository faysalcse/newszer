package com.newszer.android.model;

public class Comments {
    String userName;
    String userImage;
    String CommentMsg;
    String hour;
    String likes;
    String replay;

    public Comments(String userName, String userImage, String commentMsg, String hour, String likes, String replay) {
        this.userName = userName;
        this.userImage = userImage;
        CommentMsg = commentMsg;
        this.hour = hour;
        this.likes = likes;
        this.replay = replay;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getCommentMsg() {
        return CommentMsg;
    }

    public void setCommentMsg(String commentMsg) {
        CommentMsg = commentMsg;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getReplay() {
        return replay;
    }

    public void setReplay(String replay) {
        this.replay = replay;
    }
}
