package com.newszer.android.model;

public class Notifications {
    String date;
    String category;
    String title;
    String details;
    String notiThumnial;

    public Notifications(String date, String category, String title, String details, String notiThumnial) {
        this.date = date;
        this.category = category;
        this.title = title;
        this.details = details;
        this.notiThumnial = notiThumnial;
    }

    public Notifications(String date, String category, String details, String notiThumnial) {
        this.date = date;
        this.category = category;
        this.details = details;
        this.notiThumnial = notiThumnial;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getNotiThumnial() {
        return notiThumnial;
    }

    public void setNotiThumnial(String notiThumnial) {
        this.notiThumnial = notiThumnial;
    }
}
