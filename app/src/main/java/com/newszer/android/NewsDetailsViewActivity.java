package com.newszer.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.newszer.R;

import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NewsDetailsViewActivity extends AppCompatActivity {

    WebView webView;
    ProgressBar progressBar;

    @BindView(R.id.backBtn)
    ImageView backBtn;

    @BindView(R.id.copyBtn)
    ImageView copyBtn;


    @BindView(R.id.userComments)
    ImageView userComments;

    @BindView(R.id.likeButtonBtn)
    ImageView likeButtonBtn;

    @BindView(R.id.dislikeBtn)
    ImageView dislikeBtn;

    @BindView(R.id.linkView)
    EditText linkView;

    @BindView(R.id.getComment)
    EditText getComment;



    String myUrl;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        setContentView(R.layout.news_details_view_activity);
        ButterKnife.bind(this);


        hideNavigationBarAndroid();

        Intent intent=getIntent();

        if (intent !=null){
            myUrl=intent.getStringExtra("url_news");
        }
        linkView.setText(sortingUrl(myUrl));
        loadWebView(myUrl);

        initBannerAds();

        getComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(NewsDetailsViewActivity.this, "This app is under development", Toast.LENGTH_SHORT).show();
            }
        });

        userComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(NewsDetailsViewActivity.this, "This app is under development", Toast.LENGTH_SHORT).show();
            }
        });

        likeButtonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(NewsDetailsViewActivity.this, "This app is under development", Toast.LENGTH_SHORT).show();
            }
        });

        dislikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(NewsDetailsViewActivity.this, "This app is under development", Toast.LENGTH_SHORT).show();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        copyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText(myUrl);
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", myUrl);
                    clipboard.setPrimaryClip(clip);
                }
                Toast.makeText(NewsDetailsViewActivity.this, "Link copied . Now can paste it anywhere", Toast.LENGTH_SHORT).show();

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, myUrl);
                startActivity(Intent.createChooser(sharingIntent, "Choose one"));
            }
        });

    }

    private void initBannerAds() {
        adView = new AdView(this, getString(R.string.bannerads_placement_id), AdSize.BANNER_HEIGHT_50);

        // Find the Ad Container
        LinearLayout adContainer = (LinearLayout) findViewById(R.id.banner_container);

        // Add the ad view to your activity layout
        adContainer.addView(adView);

        // Request an ad
        adView.loadAd();
    }

    private void loadWebView(String url){
        webView = (WebView)findViewById(R.id.myWebView);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        webView.setWebChromeClient(new WebChromeClient());

        progressBar.setVisibility(View.VISIBLE);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                copyBtn.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(0);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
                progressBar.setProgress(100);
                copyBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
               // Toast.makeText(getApplicationContext(), "Cannot load page", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                copyBtn.setVisibility(View.GONE);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(!url.equals(myUrl)){//here you compare the url you click
                    linkView.setText(sortingUrl(url));
                     webView.loadUrl(url);
                     myUrl=url;
                }
                return true;
            }
        });

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(url);
        //webView.setWebViewClient(new MyWebViewClient());
    }

    public static String sortingUrl(String getUrl){
        try
        {
            URL url = new URL(getUrl);
            String baseUrl = url.getProtocol() + "://" + url.getHost();
            return baseUrl;
        }
        catch (MalformedURLException e)
        {
           return getUrl;
        }
    }

    private void hideNavigationBarAndroid() {
        if(Build.VERSION.SDK_INT < 19){
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else {
            //for higher api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }



}
